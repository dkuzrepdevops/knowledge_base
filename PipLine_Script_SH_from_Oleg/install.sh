#!/usr/bin/bash
homeDirectory=""
#pathToInstallSettingsFile="./develop/settings.cfg"

#source $pathToInstallSettingsFile

downloadJarFromNexus() {

                        mkdir -p $resourcesFolder/$1
                        chown $username:$groupname $resourcesFolder/$1

                        curl --user $nexus_username:$nexus_password -k https://$nexus_address/$1/$2/$1-$2-distrib.jar --output $resourcesFolder/$1/$1-$2.jar &> /dev/null

                                if [ $? != 0 ];
                                then

                                        formattedEcho "$tab2" RED "[ERROR] An error occurred when downloading a jar file from Nexus for the service: $1"
                                else

                                        formattedEcho "$tab2" GREEN "[INFO] Jar file successfully downloaded for the service: $1"
                                fi

                        chown $username:$groupname $resourcesFolder/$1/$1-$2.jar
                        chmod $jarPermissions $resourcesFolder/$1/$1-$2.jar

}


downloadAllJarsFromNexus() {
        formattedEcho "$tab1" BLUE "[INFO] Download jar file from Nexus"

        if [[ $EUID -ne 0 ]];
        then
               formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
               exit 1
        else

                for t in ${allService[@]}; do
                        productName=$(echo $t | awk -F ":" '{print $1}')
                        productVersion=$(echo $t | awk -F ":" '{print $2}')
                        downloadJarFromNexus $productName $productVersion
                done
        fi

}

function startApplication() {

formattedEcho "$tab1" BLUE "[INFO] Start application "

if [[ $EUID -ne 0 ]];
        then
                formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
                exit 1

        else
                if [ -f $resourcesFolder/$1/process_id ]
                then
                        formattedEcho "$tab2" BLUE "[INFO] The process is already running: $1"
                else
                        case "$1" in
                        centralingress)
                                formattedEcho "$tab1" BLUE "[INFO] Preparing to launch the service: $1"
                                startKey=$centralingressStartKey
                        ;;
                        centralegress)
                                formattedEcho "$tab1" BLUE "[INFO] Preparing to launch the service: $1"
                                startKey=$centralegressStartKey
                        ;;
                        centralproducer)
                                formattedEcho "$tab1" BLUE "[INFO] Preparing to launch the service: $1"
                                if [ "$stand" = "eDz-feature" ];
                                then
                                        startKey="$centralproducerStartKey -Djdk.security.allowNonCaAnchor=true"
                                else
                                        startKey=$centralproducerStartKey
                                fi
                        ;;
                        filter)
                                formattedEcho "$tab1" BLUE "[INFO] Preparing to launch the service: $1"
                                startKey=$filterStartKey
                        ;;
                        fullkeeper)
                                formattedEcho "$tab1" BLUE "[INFO] Preparing to launch the service: $1"
                                startKey=$fullkeeperStartKey
                        ;;
                        garant)
                                formattedEcho "$tab1" BLUE "[INFO] Preparing to launch the service: $1"
                                startKey=$garantStartKey
                        ;;
                        centralconsumer)
                                formattedEcho "$tab1" BLUE "[INFO] Preparing to launch the service: $1"
                                if [ "$stand" = "eDz-feature" ];
                                then
                                        startKey="$centralconsumerStartKey -Djdk.security.allowNonCaAnchor=true"
                                else
                                        startKey=$centralconsumerStartKey
                                fi
                        ;;
                        *)
                                formattedEcho "$tab1" RED "[ERROR] Error in the service startup parameter: wrong service name."
                                exit 1
                        ;;

                        esac


                        formattedEcho "$tab2" GRAY "[INFO] The service will be started by the command:"
                        formattedEcho "$tab2" GRAY "nohup sudo -H -u $username bash -c \"/usr/bin/java -jar $startKey  $resourcesFolder/$1/$1-$2.jar  > $logsFolder/$releaseVersion/$1.logs & echo \$! & \" "


                        if [ "$1" = "centralegress" ];
                        then
                                formattedEcho "$tab2" BLUE "[INFO] For centralegress will be creating ticket cache $centralegressTicketCache"
                                statusTicketCache=$(sudo -H -u $username bash -c "/usr/bin/kinit -kt $centralegressKeytabPath $centralegressPrincipal -c $centralegressTicketCache & echo \$?")

                        fi
                                #echo $statusTicketCache

                        app_pid=$(sudo -H -u $username bash -c "nohup /usr/bin/java -jar $startKey  $resourcesFolder/$1/$1-$2.jar  > $logsFolder/$releaseVersion/$1.logs & echo \$! &")


                        formattedEcho "$tab2" GRAY "[INFO] Process $1 starts with the PID $app_pid"
                        formattedEcho "$tab2" GRAY "[INFO] We wait 15 seconds until the service is fully started..."
                        sleep 15

                        if ps -p $app_pid > /dev/null
                        then
                                formattedEcho "$tab2" GREEN "[INFO] The process $1 started successfully $app_pid"
                                sudo -H -u $username bash -c "echo $app_pid > $resourcesFolder/$1/process_id"
                        else
                                formattedEcho "$tab2" RED "[ERROR] Process $1 stopped before reached to steady state"
                        fi

        fi
fi
}

stopApplication() {
formattedEcho "$tab1" BLUE "[INFO] Stop application"
if [[ $EUID -ne 0 ]];
       then
               formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
               exit 1
       else
        applicationPid=$(sudo -H -u $username bash -c "cat $resourcesFolder/$1/process_id 2> /dev/null")
        formattedEcho "$tab1" BLUE "[INFO] Stopping process, details:"

        #ps -p $applicationPid

        if ps -p $applicationPid &> /dev/null
        then
                formattedEcho "$tab2" BLUE "[INFO] Going to stop process having PId $applicationPid"

                killStatus=$(sudo -H -u $username bash -c "kill -9 $applicationPid; echo \$? ")

                if [ $killStatus -eq 0 ]; then
                        formattedEcho "$tab2" GREEN "[INFO] Process stopped successfully"
                else
                        formattedEcho "$tab2" RED "[ERROR] Failed to stop process having PID $applicationPid"
                fi

                rmPidFile=$(sudo -H -u $username bash -c "rm $resourcesFolder/$1/process_id; echo \$? " )

                if [ $rmPidFile -eq 0 ]; then
                        formattedEcho "$tab2" GREEN "[INFO] Pid file delete successfully"
                else
                        formattedEcho "$tab2" RED "[ERROR] Failed to delete pid file  $resourcesFolder/$1/process_id"
                fi

        else
                formattedEcho "$tab2" RED "[ERROR] Failed to stop process, Process is not running"

        fi

fi
}

formattedEcho() {
  if [ "$colorizeEcho" = "true" ];
  then
        # tab=$1
        # case "$2" in
        # RED)
        #   echo -e "$tab ${RED} $3  ${NC}"
        # ;;
        # BLUE)
        #   echo -e "$tab ${BLUE} $3  ${NC}"
        # ;;
        # GREEN)
        #   echo -e "$tab ${GREEN} $3  ${NC}"
        # ;;
        # GRAY)
        #   echo -e "$tab ${GRAY} $3  ${NC}"
        # ;;
        # *)
        #   echo -e "$tab $3"
        # ;;

        tab=$1
        case "$2" in
        RED)
          echo -e "$tab \033[35m $3  \033[0m"
        ;;
        BLUE)
          echo -e "$tab \033[32m $3  \033[0m"
        ;;
        GREEN)
          echo -e "$tab \033[32m $3  \033[0m"
        ;;
        GRAY)
          echo -e "$tab \033[32m $3  \033[0m"
        ;;
        *)
          echo -e "$tab $3"
        ;;

        esac
  else
        tab=$1
        case "$2" in
        RED)
        echo -e "$tab $3 "
        ;;
        BLUE)
        echo -e "$tab $3 "
        ;;
        GREEN)
        echo -e "$tab $3 "
        ;;
        GRAY)
        echo -e "$tab $3 "
        ;;
        *)
        echo -e "$tab $3 "
        ;;

        esac
  fi

}


installRequirements() {
        formattedEcho "$tab1" BLUE "[INFO] Install Requirements"
        if [[ $EUID -ne 0 ]];
                then
                        formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
                        exit 1
                else
                        formattedEcho "$tab1" BLUE "[INFO] Start install requirements"


                        for directory in ${allDirectoryStructure[@]}; do
                                if [ -d $directory ]
                                        then
                                                formattedEcho "$tab2" BLUE "[INFO] The directory already exists: $directory"
                                        else
                                                mkdir -p $directory
                                                formattedEcho "$tab2" GREEN "[SUCCESS] The directory was created successfully: $directory"
                                fi
                        done

                        for directory in ${allDirectoryStructure[@]}; do
                                if [ "$(stat -c %U $directory)" = "$username" ];
                                then
                                        formattedEcho "$tab2" BLUE "[INFO] The directory has the correct permissions: $directory"
                                else
                                        chown $username:$groupname $directory
                                        formattedEcho "$tab2" GREEN "[SUCCESS] The directory has been granted the correct permissions: $directory"
                                fi
                        done

        fi
}

setCorrectPermissions() {
    formattedEcho "$tab1" BLUE "[INFO] Set corrent Permissions"


    formattedEcho "$tab1" BLUE "[INFO] Set keytab permissions"

    for keytab in ${allKeytabsStructure[@]}; do

            chown $username:$groupname $keytab
            chmod $certificatePermissions $keytab

            if [ "$(stat -c %U $keytab)" = "$username" ];
                    then
                            formattedEcho "$tab2" BLUE "[INFO] The keytab has the correct permissions: $keytab"
                    else
                            formattedEcho "$tab2" RED "[ERROR] The keytab has incorrect permissions: $keytab"
                            #exit 1
            fi
    done

    formattedEcho "$tab1" BLUE "[INFO] Set all certificate permissions"

     for certificate in ${allCertificatesStructure[@]}; do

            chown $username:$groupname $certificate
            chmod $certificatePermissions $certificate

            if [ "$(stat -c %U $certificate)" = "$username" ];
            then
                    formattedEcho "$tab2" BLUE "[INFO] The certificate has the correct permissions: $certificate"
            else
                    formattedEcho "$tab2" RED "[ERROR] The certificate has incorrect permissions: $certificate"
            fi
    done


}

checkRequirements() {
if [[ $EUID -ne 0 ]];
        then
                formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
                exit 1

        else

        formattedEcho "$tab1" BLUE "[INFO] Check requirements"

        formattedEcho "$tab1" BLUE "[INFO] Check all directory structure"

        for directory in ${allDirectoryStructure[@]}; do

                if [ -d $directory ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The directory has been created: $directory"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The directory was not created: $directory"
                                exit 1
                fi
        done

        for directory in ${allDirectoryStructure[@]}; do
                if [ "$(stat -c %U $directory)" = "$username" ];
                then
                        formattedEcho "$tab2" BLUE "[INFO] The directory has the correct permissions: $directory"
                else
                        formattedEcho "$tab2" RED "[ERROR] The directory has incorrect permissions: $directory"
                fi
        done

        formattedEcho "$tab1" BLUE "[INFO] Check all certificate structure"

        for certificate in ${allCertificatesStructure[@]}; do

                if [ -f $certificate ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The certificate exists: $certificate"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The certificate does not exists: $certificate"
                                #exit 1
                fi
        done

        formattedEcho "$tab1" BLUE "[INFO] Check all keytab structure"

        for keytab in ${allKeytabsStructure[@]}; do

                if [ -f $keytab ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The keytab exists: $keytab"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The keytab does not exists: $keytab"
                                #exit 1
                fi
        done

        formattedEcho "$tab1" BLUE "[INFO] Check keytab permissions"

        for keytab in ${allKeytabsStructure[@]}; do

                if [ "$(stat -c %U $keytab)" = "$username" ];
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The keytab has the correct permissions: $keytab"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The keytab has incorrect permissions: $keytab"
                                #exit 1
                fi
        done

        formattedEcho "$tab1" BLUE "[INFO] Check all certificate permissions"

         for certificate in ${allCertificatesStructure[@]}; do
                if [ "$(stat -c %U $certificate)" = "$username" ];
                then
                        formattedEcho "$tab2" BLUE "[INFO] The certificate has the correct permissions: $certificate"
                else
                        formattedEcho "$tab2" RED "[ERROR] The certificate has incorrect permissions: $certificate"
                fi
        done

        formattedEcho "$tab1" BLUE "[INFO] Password verification certificates"

        for pair in ${allJksStructure[@]}; do
                pathToJks=$(echo $pair | awk -F ":" '{print $1}')
                passwordJks=$(echo $pair | awk -F ":" '{print $2}')
                keytoolStatus=$(sudo -H -u $username bash -c "keytool -list -rfc -keystore $pathToJks -storepass $passwordJks &> /dev/null; echo \$? ")

                if [ $keytoolStatus == 1 ];
                then

                        formattedEcho "$tab2" RED "[ERROR] The JKS has incorrect permissions: $pathToJks"
                else

                        formattedEcho "$tab2" BLUE "[INFO] The JKS has the correct permissions: $pathToJks"

                        if [ $keytoolStatus != 0 ];
                        then

                                formattedEcho "$tab2" RED "[ERROR] Incorrect password for JKS: $pathToJks"
                        else

                                formattedEcho "$tab2" BLUE "[INFO] The password is correct for JKS: $pathToJks"
                        fi
                fi
        done

        formattedEcho "$tab1" BLUE "[INFO] Check log directory exists"

                if [[ -d $logsFolder && -d $logsFolder/$releaseVersion ]]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The logs directories [ $logsFolder; $logsFolder/$releaseVersion ] are created."
                        else
                                formattedEcho "$tab2" RED "[ERROR] One of the directories [ $logsFolder; $logsFolder/$releaseVersion ] was not created"
                fi

                if [[ -L /data/applogs/$logsAppId &&  -e /data/applogs/$logsAppId ]]
                then
                        formattedEcho "$tab2" BLUE "[INFO] The link /data/applogs/$logsAppId valide."
                else
                        formattedEcho "$tab2" RED "[ERROR] The link /data/applogs/$logsAppId not valide."
                fi

        formattedEcho "$tab1" BLUE "[INFO] Check crontab correct"

                if grep -Fxq "$centralegressTicketCacheCron  /usr/bin/kinit -kt $centralegressKeytabPath $centralegressPrincipal -c $centralegressTicketCache"  /var/spool/cron/$username
                then
                        formattedEcho "$tab2" BLUE "[INFO] The crontab is correct."
                else
                        formattedEcho "$tab2" RED "[ERROR] The crontab is incorect"
                fi

        formattedEcho "$tab1" BLUE "[INFO] Check logrotate exist"

                if [ -f /etc/logrotate.d/eventbroker ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The logrotate exist"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The logrotate not exists"

                fi

        formattedEcho "$tab1" BLUE "[INFO] Check ticket cache exist"

                if [ -f $centralegressTicketCache ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The ticket cache exist"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The ticket cache exists"

                fi

fi
}

startAllApplication() {
        if [[ $EUID -ne 0 ]];
        then
                formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
                exit 1

        else
                for t in ${allService[@]}; do
                        productName=$(echo $t | awk -F ":" '{print $1}')
                        productVersion=$(echo $t | awk -F ":" '{print $2}')

                        startApplication $productName $productVersion
                done
        fi

}

stopAllApplication() {
        if [[ $EUID -ne 0 ]];
        then
                formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
                exit 1

        else
                for t in ${allService[@]}; do
                        productName=$(echo $t | awk -F ":" '{print $1}')
                        productVersion=$(echo $t | awk -F ":" '{print $2}')
                        stopApplication $productName $productVersion
                done
        fi

}

statusApplication(){
formattedEcho "$tab1" BLUE "[INFO] Start application "

                if [ -f $resourcesFolder/$1/process_id ]
                then
                        formattedEcho "$tab2" BLUE "[INFO] The service is running: $1  - PID $(cat $resourcesFolder/$1/process_id) "
                else
                        formattedEcho "$tab2" BLUE "[INFO] The service is not running: $1"
                fi

}
statusAllApplication() {
if [[ $EUID -ne 0 ]];
        then
                formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
                exit 1

        else
                for t in ${allService[@]}; do
                        productName=$(echo $t | awk -F ":" '{print $1}')
                        productVersion=$(echo $t | awk -F ":" '{print $2}')
                        statusApplication $productName $productVersion
                done
        fi

}

createLogsLinks() {
if [[ $EUID -ne 0 ]];
        then
                formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
                exit 1

        else
                if [[ -d $logsFolder && -d $logsFolder/$releaseVersion ]]
                        then
                                formattedEcho "$tab1" BLUE "[INFO] The logs directories [ $logsFolder; $logsFolder/$releaseVersion ] are created. Now we will create links"
                                rm -f /data/applogs/$logsAppId
                                ln -s $logsFolder/$releaseVersion /data/applogs/$logsAppId
                        else
                                formattedEcho "$tab1" RED "[ERROR] One of the directories [ $logsFolder; $logsFolder/$releaseVersion ] was not created"
                                exit 1
                fi
fi
}

installCrontab() {
if [[ $EUID -ne 0 ]];
        then
                formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
                exit 1

        else
                if grep -Fxq "$centralegressTicketCacheCron  /usr/bin/kinit -kt $centralegressKeytabPath $centralegressPrincipal -c $centralegressTicketCache"  /var/spool/cron/$username
                then
                        formattedEcho "$tab1" BLUE "[INFO] Crontab already exist"
                else
                        echo "$centralegressTicketCacheCron  /usr/bin/kinit -kt $centralegressKeytabPath $centralegressPrincipal -c $centralegressTicketCache" >> /var/spool/cron/$username
                        formattedEcho "$tab1" GREEN "[INFO] Crontab successfully created"
                fi

fi
}

createTicketCache(){
if [[ $EUID -ne 0 ]];
        then
                formattedEcho "$tab1" RED "[ERROR] This command must be run with sudo"
                exit 1

        else

                createTicketCacheStatus=$(sudo -H -u $username bash -c "/usr/bin/kinit -kt $centralegressKeytabPath $centralegressPrincipal -c $centralegressTicketCache &> /dev/null; echo \$? " )

                if [ $createTicketCacheStatus != 0 ];
                then
                        formattedEcho "$tab1" RED "[ERROR] Error during cache creation"
                else
                        formattedEcho "$tab1" BLUE "[INFO] The cache was created successfully"
                fi

fi

}

installLogrotate() {
        echo -e $logrotateRules > /etc/logrotate.d/eventbroker
}

installTargetcosumer() {
        formattedEcho "$tab2" BLUE "[INFO] Create direcrory ~/targetconsumer"
        mkdir -p $targetconsumerFolder

        formattedEcho "$tab2" BLUE "[INFO] Create diractory ~/targetconsumer/logs"
        mkdir -p $targetconsumerLogFolder

        formattedEcho "$tab2" BLUE "[INFO] Download targetconsumerVersion "

        curl --user $nexus_username:$nexus_password -k https://$nexus_address/targetconsumer/$targetconsumerVersion/targetconsumer-$targetconsumerVersion-distrib.jar --output $targetconsumerFolder/targetconsumer-$targetconsumerVersion.jar &> /dev/null

                                if [ $? != 0 ];
                                then

                                        formattedEcho "$tab2" RED "[ERROR] An error occurred when downloading a jar file from Nexus for the service: targetconsumer"
                                else

                                        formattedEcho "$tab2" GREEN "[INFO] Jar file successfully downloaded for the service: targetconsumer"
                                fi
}

checkTargetcosumer() {

                formattedEcho "$tab2" BLUE "[INFO] Check targetconsumer folder "
                if [[ -d $targetconsumerFolder && -d $targetconsumerLogFolder ]]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The logs directories [ $targetconsumerFolder; $targetconsumerLogFolder ] are created. Now we will create links"
                        else
                                formattedEcho "$tab2" RED "[ERROR] One of the directories [ $targetconsumerFolder; $targetconsumerLogFolder ] was not created"
                fi


                formattedEcho "$tab2" BLUE "[INFO] Check targetconsumer jar "
                if [ -f $targetconsumerFolder/targetconsumer-$targetconsumerVersion.jar ]
                then
                        formattedEcho "$tab2" BLUE "[INFO] Jar file $targetconsumerFolder/targetconsumer-$targetconsumerVersion.jar exist"
                else
                        formattedEcho "$tab2" RED "[ERROR] Jar file $targetconsumerFolder/targetconsumer-$targetconsumerVersion.jar not exist"
                fi

                formattedEcho "$tab2" BLUE "[INFO] Check exist targetconsumer truststore "
                if [ -f $targetconsumerTruststore ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The certificate exists: $targetconsumerTruststore"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The certificate does not exists: $targetconsumerTruststore"
                fi

                formattedEcho "$tab2" BLUE "[INFO] Check exist targetconsumer keystore "
                if [ -f $targetconsumerKeystore ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The certificate exists: $targetconsumerKeystore"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The certificate does not exists: $targetconsumerKeystore"
                fi

                formattedEcho "$tab2" BLUE "[INFO] Password varification for truststore and keystore "
                truststoreStatus=$(keytool -list -rfc -keystore $targetconsumerTruststore -storepass $targetconsumerTruststorePassword &> /dev/null; echo $? )
                keystoreStatus=$(keytool -list -rfc -keystore $targetconsumerKeystore -storepass $targetconsumerKeystorePassword &> /dev/null; echo $? )

                if [[ $truststoreStatus != 0 && $keystoreStatus != 0 ]];
                then

                                formattedEcho "$tab2" RED "[ERROR] Incorrect password for truststore: $targetconsumerTruststore or keystore: $targetconsumerKeystore "
                else

                                formattedEcho "$tab2" BLUE "[INFO] The password for truststore: $targetconsumerTruststore and keystore: $targetconsumerKeystore correct "
                fi

                createKeytabStatus=$(/usr/sbin/ipa-getkeytab -k $targetconsumerKeytab -p $targetconsumerPrincipal -s $targetconsumerIpaServer &> /dev/null; echo $?)

                if [ $createKeytabStatus != 0 ];
                then

                                formattedEcho "$tab2" RED "[ERROR] Error during keytab creation :$targetconsumerKeytab "
                else

                                formattedEcho "$tab2" BLUE "[INFO] The keytab was created successfully: $targetconsumerKeytab "
                fi

                formattedEcho "$tab2" BLUE "[INFO] Check exist targetconsumer keytab "
                #echo $targetconsumerKeytab
                if [ -f $targetconsumerKeytab ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The keytab exists: $targetconsumerKeytab"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The keytab does not exists: $targetconsumerKeytab"
                fi


}

startTargetcosumer() {
  formattedEcho "$tab1" BLUE "[INFO] Start application targetconsumer"

  app_pid=$(nohup /usr/bin/java -jar $targetconsumerStartKey $targetconsumerFolder/targetconsumer-$targetconsumerVersion.jar > $targetconsumerLogFolder/targetconsumer.logs & echo $! &)

  formattedEcho "$tab2" GRAY "[INFO] Process starts with the PID $app_pid"
  formattedEcho "$tab2" GRAY "[INFO] We wait 15 seconds until the service is fully started..."
  sleep 15

  if ps -p $app_pid > /dev/null
  then
         formattedEcho "$tab2" GREEN "[INFO] The process  started successfully $app_pid"
         echo $app_pid > $targetconsumerFolder/process_id
  else
         formattedEcho "$tab2" RED "[ERROR] Process stopped before reached to steady state"
  fi
}

stopTargetconsumer() {
        applicationPid=$(cat $targetconsumerFolder/process_id 2> /dev/null)
        formattedEcho "$tab1" BLUE "[INFO] Stopping process, details:"

        if ps -p $applicationPid &> /dev/null
        then
                formattedEcho "$tab2" BLUE "[INFO] Going to stop process having PId $applicationPid"

                killStatus=$(kill -9 $applicationPid; echo $? )

                if [ $killStatus -eq 0 ]; then
                        formattedEcho "$tab2" GREEN "[INFO] Process stopped successfully"
                else
                        formattedEcho "$tab2" RED "[ERROR] Failed to stop process having PID $applicationPid"
                fi

                rmPidFile=$(rm -f $targetconsumerFolder/process_id; echo $?  )

                if [ $rmPidFile -eq 0 ]; then
                        formattedEcho "$tab2" GREEN "[INFO] Pid file delete successfully"
                else
                        formattedEcho "$tab2" RED "[ERROR] Failed to delete pid file  $resourcesFolder/$1/process_id"
                fi

        else
                formattedEcho "$tab2" RED "[ERROR] Failed to stop process, Process is not running"

        fi
}

showResultFromTargetconsumer() {
        echo "showResultFromTargetconsumer"
}

installTargetproducer() {
   formattedEcho "$tab2" BLUE "[INFO] Create direcrory $targetproducerFolder"
   mkdir -p $targetproducerFolder

   formattedEcho "$tab2" BLUE "[INFO] Create diractory $targetproducerCertificatesFolder"
   mkdir -p $targetconsumerLogFolder


}

checkTargetproducer() {
        formattedEcho "$tab2" BLUE "[INFO] Check targetproducer folder "
                if [[ -d $targetproducerFolder && -d $targetproducerCertificatesFolder ]]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The directories [ $targetproducerFolder; $targetproducerCertificatesFolder ] are created. Now we will create links"
                        else
                                formattedEcho "$tab2" RED "[ERROR] One of the directories [ $targetproducerFolder; $targetproducerCertificatesFolder ] was not created"
                fi

                formattedEcho "$tab2" BLUE "[INFO] Check exist targetproducer $targetproducerFolderServerPem "
                if [ -f $targetproducerFolderServerPem ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The certificate exists: $targetproducerFolderServerPem"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The certificate does not exists: $targetproducerFolderServerPem"
                fi

                formattedEcho "$tab2" BLUE "[INFO] Check exist targetproducer $targetproducerFolderClientP12 "
                if [ -f $targetproducerFolderClientP12 ]
                        then
                                formattedEcho "$tab2" BLUE "[INFO] The certificate exists: $targetproducerFolderClientP12"
                        else
                                formattedEcho "$tab2" RED "[ERROR] The certificate does not exists: $targetproducerFolderClientP12"
                fi
}

showTargetconsumerResult() {
    logsLine=$(grep "ReciveController" ./targetconsumer/logs/targetconsumer.logs)
    formattedEcho "$tab0" GRAY "$logsLine"
}

runScenario() {
    testEndpoint
    testCase1
    testCase2
}

checkResultCurl() {
    outputCurl=$(echo $1 | awk -F "|" '{print $1}')
    statusCodeCurl=$(echo $1 | awk -F "|" '{print $2}')


    if [ $statusCodeCurl == 200 ];
    then
        formattedEcho "$tab2" GREEN "[INFO] Result is:"
        formattedEcho "$tab2" GREEN "       $outputCurl"
    else
        formattedEcho "$tab2" RED "[ERROR] Result is:"
        formattedEcho "$tab2" RED "        $outputCurl"
    fi
}

testEndpoint() {
    formattedEcho "$tab2" BLUE "[INFO] Check /status-check endpoint"

      resultCurl=$(/usr/bin/curl --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X GET https://$targetconsumerCentralingress/messageservice/status-check  )
      checkResultCurl "$resultCurl"


    formattedEcho "$tab2" BLUE "[INFO] Check /heartbeat-microservices endpoint"

      resultCurl=$(/usr/bin/curl --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X GET https://$targetconsumerCentralingress/messageservice/heartbeat-microservices  )
      checkResultCurl "$resultCurl"


    formattedEcho "$tab2" BLUE "[INFO] Check /messageservice/get-subscribes-for-host endpoint "

      resultCurl=$(/usr/bin/curl --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json'  https://$targetconsumerCentralingress/messageservice/get-subscribes-for-host  -d  '{"listener_host":"'$targetconsumerListenerHost'"}'  )
      checkResultCurl "$resultCurl"

}

testCase1() {

    topicName=test_autocheck_topic_$(shuf -i 2000-65000 -n 1)

    formattedEcho "$tab2" BLUE "[INFO] Subscribe topic_name: $topicName"

      resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/topic-listener -d  '{"topic_name":"'$topicName'", "user_name":"autocheck", "listener_host":"'$targetconsumerListenerHost'", "listener_port":"'$targetconsumerListenerPort'", "listener_api":"get-topic" }' )
      checkResultCurl "$resultCurl"


    formattedEcho "$tab2" BLUE "[INFO] Send 5 ENG message "

      for i in {1..5}
      do
          resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/send-topic/$topicName -d  '{ "info_field":"TEST_FROM_SCRIPT_1","anykey'$i'":"val_'$i'" }' )
          checkResultCurl "$resultCurl"
      done



    formattedEcho "$tab2" BLUE "[INFO] Send 5 RUS message "

      for i in {1..5}
      do
          resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/send-topic/$topicName -d  '{ "info_field":"ТЕСТ_ИЗ_СКРИПТА_1","anykey'$i'":"val_'$i'" }' )
          checkResultCurl "$resultCurl"
      done

    formattedEcho "$tab2" BLUE "[INFO] Unsubscribe topic_name: $topicName"

      resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/delete-topic-listener -d '{ "topic_name":"'$topicName'", "user_name":"autocheck" }'  )
      checkResultCurl "$resultCurl"


}


testCase2() {

topicName=test_autocheck_filter_$(shuf -i 2000-65000 -n 1)
filterName=$topicName

  formattedEcho "$tab2" BLUE "[INFO] Subscribe topic_name: $topicName"

    resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/topic-listener -d  '{"topic_name":"'$topicName'", "user_name":"autocheck", "listener_host":"'$targetconsumerListenerHost'", "listener_port":"'$targetconsumerListenerPort'", "listener_api":"get-topic" }' )
    checkResultCurl "$resultCurl"


  formattedEcho "$tab2" BLUE "[INFO] Add 1 filter"

    resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/create-new-filter -d  '{ "creatorHostname":"10.88.88.88","creatorRole":"ADMIN", "consumerName":"autocheck", "consumerAPI":"https://'$targetconsumerListenerHost':'$targetconsumerListenerPort'/get-topic", "topicName":"'$topicName'", "filterName":"'$filterName'_1","filterBody":"$..[?(@.anykey3)]" }' )
    checkResultCurl "$resultCurl"

  formattedEcho "$tab2" BLUE "[INFO] Add 2 filter"

    resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/create-new-filter -d  '{ "creatorHostname":"10.88.88.88","creatorRole":"ADMIN", "consumerName":"autocheck", "consumerAPI":"https://'$targetconsumerListenerHost':'$targetconsumerListenerPort'/get-topic", "topicName":"'$topicName'", "filterName":"'$filterName'_2","filterBody":"$..[?(@.umpalumpa)]" }' )
    checkResultCurl "$resultCurl"


  formattedEcho "$tab2" BLUE "[INFO] Send 5 message with filter"

    for i in {1..5}
    do
        resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/send-topic/$topicName -d  '{ "info_field":"TEST_FROM_SCRIPT_WITH_FILTER","anykey'$i'":"val_'$i'" }' )
        checkResultCurl "$resultCurl"
    done

  formattedEcho "$tab2" BLUE "[INFO] Delete one filter "
        resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/delete-filter  -d  '{ "consumerAPI":"https://'$targetconsumerListenerHost':'$targetconsumerListenerPort'/get-topic", "consumerName":"autocheck", "topicName":"'$topicName'", "filterName":"'$filterName'_1" }' )
        checkResultCurl "$resultCurl"

  formattedEcho "$tab2" BLUE "[INFO] Delete all "
        resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/delete-filters-for-topic  -d  '{ "consumerAPI":"https://'$targetconsumerListenerHost':'$targetconsumerListenerPort'/get-topic", "consumerName":"autocheck", "topicName":"'$topicName'"  }' )
        checkResultCurl "$resultCurl"

  formattedEcho "$tab2" BLUE "[INFO] Send 5 message without filter"

    for i in {1..5}
    do
        resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/send-topic/$topicName -d  '{ "info_field":"TEST_FROM_SCRIPT_WITHOUT_FILTER","anykey'$i'":"val_'$i'" }' )
        checkResultCurl "$resultCurl"
    done

  formattedEcho "$tab2" BLUE "[INFO] Unsubscribe"
    resultCurl=$(/usr/bin/curl  --write-out '|%{http_code}' --silent --cacert $targetproducerFolderServerPem --cert $targetproducerFolderClientP12 -k -L --negotiate -u : -X POST -H 'Content-Type: application/json' https://$targetconsumerCentralingress/messageservice/delete-topic-listener -d '{ "topic_name":"'$topicName'", "user_name":"autocheck" }'  )
    checkResultCurl "$resultCurl"
}

colorizeEcho="true"
formattedEcho "$tab0" BLUE "[INFO] Start EventBroker instalation script"
pathToInstallSettingsFile="$2"
source $pathToInstallSettingsFile


        case "$1" in
        check)
                checkRequirements
        ;;
        install)
                installRequirements
                downloadAllJarsFromNexus
                setCorrectPermissions
                createLogsLinks
                installCrontab
                installLogrotate
                createTicketCache
        ;;
        start)
                createTicketCache
                startAllApplication
                #startApplication centralingress 1.3.17.1
        ;;
        stop)
                stopAllApplication
                #stopApplication centralingress 1.3.17.1
        ;;
        status)
                statusAllApplication
        ;;
        backup)
                echo "drafting"
        ;;
        test_start)
                installTargetcosumer
                checkTargetcosumer
                installTargetproducer
                checkTargetproducer
                startTargetcosumer
        ;;
        test_run)
                runScenario
        ;;
        test_show)
                showTargetconsumerResult
        ;;
        test_stop)
                stopTargetconsumer
        ;;
        *)
                echo "usage: $0 check | install | start | stop | backup" >&2
                exit 1
        ;;
        esac
